--  creating system user and giving connection permissions
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvd_rental TO rentaluser;

-- Grant SELECT permission to rentaluser on the customer table
GRANT SELECT ON TABLE public.customer TO rentaluser;


-- creating a user group(role)
CREATE ROLE rental;

-- adding the user to the group
GRANT rental TO rentaluser;

-- Grant INSERT and UPDATE permissions to the "rental" group for the "rental" table
GRANT INSERT, UPDATE ON TABLE public.rental TO rental;

-- after connecting as rental role
insert into public.rental(customer_id, inventory_id, staff_id, rental_date, return_date)
values (1, 1, 1, '2024.12.03'::date, '2024.12.08'::date);
update public.rental
set return_date='2024.12.12'::date
where rental_date = '2024.12.03';

REVOKE INSERT FROM rental;
insert into public.rental(customer_id, inventory_id, staff_id, rental_date, return_date)
values (1, 1, 1, '2024.10.03'::date, '2024.10.08'::date);


--  adding generated column to ensure each user gets their own data
alter table public.customer
    add column user_role varchar generated always as ( 'client_' || first_name || '_' || last_name) STORED;

-- creating base role
CREATE ROLE base_user_role noinherit;
GRANT SELECT on public.customer, public.rental,public.payment to base_user_role;

-- function to get current session customer id
create function retrieve_customer_id() returns integer
    language plpgsql
as
$$
BEGIN
    RETURN (
        SELECT customer_id
        FROM public.customer
        WHERE user_role = current_user
    );
END;
$$;


DO
$$

    DECLARE
        customer_row RECORD;
        role_name    VARCHAR(100);
    BEGIN
        -- Loop through each customer
        FOR customer_row IN SELECT * FROM customer
            LOOP
                -- Generate the role name
                role_name := 'client_' || customer_row.first_name || '_' || customer_row.last_name;

                -- Check if the role already exists
                IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = role_name) THEN
                    -- Create personalized role
                    EXECUTE 'CREATE ROLE ' || quote_ident(role_name) || ' with login password ' ||
                            quote_literal(customer_row.email) || ' INHERIT';
                    EXECUTE 'GRANT base_user_role to ' || quote_ident(role_name);
                END IF;
            END LOOP;
    END
$$;


-- Define a row-level security policy to restrict access to their own data
CREATE POLICY customer_payment_isolation_policy
    ON public.payment
    FOR SELECT
    USING ("customer_id" = retrieve_customer_id());

CREATE POLICY customer_rental_isolation_policy
    ON public.rental
    FOR SELECT
    USING ("customer_id" = retrieve_customer_id());




